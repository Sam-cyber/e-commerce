import React, { Component } from 'react'
import './App.css';
import Cart from './components/Cart';
import Filter from './components/Filter';
import Products from './components/Products';
import data from './data.json'


export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      asset: data.products,
      size: "",
      sort: "",
      cartitems: localStorage.getItem("cartitems")?
      JSON.parse(localStorage.getItem("cartitems")):[]

    }
  }

  removefromcart = (p) =>{
    const cartitems= this.state.cartitems.slice()
    this.setState({
    cartitems: cartitems.filter((x)=>x._id !== p._id)
  })
  localStorage.setItem("cartitems", JSON.stringify(cartitems.filter((x)=>x._id !== p._id)))
  }

  addtocart =(p)=>{
    const cartitems = this.state.cartitems.slice();
    let alreadyincart = false
    cartitems.forEach(item => {
      if(item._id === p._id){
        item.count++;
        alreadyincart = true
       
      }
      console.log(alreadyincart)
    });
    if(!alreadyincart){
      cartitems.push({...p, count:1})
      
    }
    this.setState({
      cartitems
    })
    localStorage.setItem("cartitems", JSON.stringify(cartitems))
   
  }

  sortproducts=(e)=>{
    const sort= e.target.value
    console.log(e.target.value)
    this.setState  ({
      sort: e.target.value,
      asset: this.state.asset.slice().sort((a,b)=>(
        sort === "Lowest"?
        ((a.price > b.price)? 1:-1):
        sort === "Highest"?
        ((a.price < b.price)? 1:-1):
        a._id>b._id ?1 :-1
         
      ))
      
    })
  }

  filterproducts=(e)=>{
    console.log(e.target.value);
    
    if (e.target.value === "All"){
      this.setState({ 
         size: e.target.value,
         asset: data.products
      })
    }else{
    this.setState({
      size: e.target.value,
      asset: data.products.filter((p) => p.availableSizes.indexOf(e.target.value)>=0, )
    })}
  }

  render() {
    return (
      <div className="grid-container">
        <header >
        <button className="login"> Login </button>
          <a href="/"> Nepali Shopping Cart </a>
        </header>
        <main>
          <div className="content">
            <div className="main"> 
            
            <Filter count={this.state.asset.length}
            size={this.state.size}
            sort={this.state.sort}
            filterproducts={this.filterproducts}
            sortproducts={this.sortproducts}
            name= "hello"/> Products List <Products sabai={this.state.asset} addtocart={this.addtocart}/></div>
           
            <div className="sidebar">
           <Cart cartitems={this.state.cartitems} removefromcart={this.removefromcart}/>
                </div>
          </div>
        </main>
        <footer>
          All rights reserved
        </footer>

      </div>
    )
  }
}








