import React, { Component } from 'react'
import formatCurrency from '../Utils'
import Fade from 'react-reveal/Fade';
import Modal from 'react-modal'
import Zoom from 'react-reveal/Zoom'

export default class Products extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             p: null 
        }
    }
    openModal=(p)=>{
        this.setState({
            p
        })
    
    
    }    
    closeModal=()=>{
        this.setState({
            p:null
        })
    
    }
    
    render() {
        const { p } = this.state
        return (
            <div>
                <Fade bottom cascade={true}>
                <ul className="products">
                    {this.props.sabai.map((p)=>(
                    <li key={p._id}>
                        <div className="product">
                        <a href={"#" + p._id}
                        onClick={()=> this.openModal(p)}>
                            <img src={p.image} alt={p.title}/>
                            <br/>{p.title}

                        </a>
                        <div className="product-price">
                            <div>
                                {formatCurrency(p.price)}
                            </div>
                            <div>
                                <button onClick={()=> this.props.addtocart(p)} className="button primary"> Add to Cart </button>
                            </div>
                        </div>
                        </div>
                    </li>
                    ))}
                </ul>
                </Fade>
                {
                    p && (
                        <Modal isOpen={true}
                                onRequestClose=
                                {this.closeModal}>
                                     <Zoom>
                                <button className="close-modal" onClick={this.closeModal}> x </button>
                               <div className="product-details">
                                <img src={p.image} alt={p.title}></img>
                                <div className="product-details-description">
                                    <p> <strong> {p.title} </strong></p>
                                    <p> {p.description}</p>
                                   
                                    <div className="product-price"> {formatCurrency(p.price)} </div>
                                </div>
                               </div>
                               
                                <div> Modal </div>
                            </Zoom>
                        </Modal>
                    )
                }
            </div>
        )
    }
}
