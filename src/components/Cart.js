import React, { Component } from 'react'
import formatCurrency from '../Utils';
import Fade from 'react-reveal'

export default class Cart extends Component {
    render() {
        const { cartitems } = this.props;
        return (
            <div>
                
               {cartitems.length === 0? (<div className="cart cart-header"> Cart is Empty </div>):
               ( <div> You have {cartitems.length} in the Cart {" "} </div> )} 

                <div>           
                <div className="cart">
                    <Fade left cascade={true}>
                    <ul className="cart-items">
                    {cartitems.map((item)=>(
                        <li key={item._id}>
                            <div>
                                <img src={item.image} alt={item.title}></img>
                            </div>
                            <div>
                                <div>{item.title}</div>
                                <div className="right">
                                    {formatCurrency (item.price)} x {item.count}
                                <button onClick={()=>this.props.removefromcart(item)}> Remove </button>
                                </div>
                            </div>
                        </li>
                    ))}
                    </ul>
                    </Fade>
                </div>

                        {cartitems.length !==0 && (

                <div className="total">
                   
                       Total:  { formatCurrency (cartitems.reduce((a,c) => a + c.price * c.count,0))}
                            
                       <button className="button primary"> Proceed </button> 
                   
                </div>
                        )}
            </div>
            </div>
            
        )
    }
}
